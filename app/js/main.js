var ui = {
	doc: $(document),
	bind: function() {
		this.doc.on('click', '[data-toggle="modal"]', function(event) {
			ui.display_image($(this));
		});

		this.doc.on('hidden.bs.modal', '#modalPreview', function(event) {
			ui.remove_image($(this));
		});
	},
	remove_image: function(modal) {
		modal.find('.modal-body').children('img').remove();
	},
	display_image: function(element) {
		var modal = element.data('target'),
			clone = element.clone().removeAttr('data-target').removeAttr('data-toggle');

			$(modal).find('.modal-body').append(clone);
	},
	console: function() {
		// Capture not just the anchor clicks but the actual clicked/tapped element
		var eventType, verb;

		if (isMobile.tablet == true || isMobile.phone == true) {
			verb = 'tapped';
			eventType = 'touchstart';
		}
		else {
			verb = 'clicked';
			eventType = 'click';
		}

		this.doc.on(eventType, 'html', function(event) {
			var nodeName = $(event.target).prop('nodeName');

			console.log('You '+verb+' ' + nodeName);
		});
	},


	build: function() {
		this.bind();
		this.console();
	}
}


$(document).ready(function() {
	ui.build();
});